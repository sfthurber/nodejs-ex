//express boilerplate and middleware declarations
var express = require('express'), app = express(), morgan = require('morgan'), cookieParser = require('cookie-parser'), cors = require('cors');    
//Object.assign = require('object-assign');
app.use(express.static("client/build"));
app.use(morgan('combined'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// openshift boilerplate -- for ports and for mongo 
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0',
    mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL,
    mongoURLLabel = "";
if (mongoURL == null && process.env.DATABASE_SERVICE_NAME) {
  var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase(),
      mongoHost = process.env[mongoServiceName + '_SERVICE_HOST'],
      mongoPort = process.env[mongoServiceName + '_SERVICE_PORT'],
      mongoDatabase = process.env[mongoServiceName + '_DATABASE'],
      mongoPassword = process.env[mongoServiceName + '_PASSWORD']
      mongoUser = process.env[mongoServiceName + '_USER'];
  if (mongoHost && mongoPort && mongoDatabase) {
    mongoURLLabel = mongoURL = 'mongodb://';
    if (mongoUser && mongoPassword) {
      mongoURL += mongoUser + ':' + mongoPassword + '@';
    }
    mongoURLLabel += mongoHost + ':' + mongoPort + '/' + mongoDatabase;
    mongoURL += mongoHost + ':' +  mongoPort + '/' + mongoDatabase;
  }
}
var db = null,
    dbDetails = new Object();
var initDb = function(callback) {
  if (mongoURL == null) return;
  var mongodb = require('mongodb');
  if (mongodb == null) return;
  mongodb.connect(mongoURL, function(err, conn) {
    if (err) {
      callback(err);
      return;
    }
    db = conn;
    dbDetails.databaseName = db.databaseName;
    dbDetails.url = mongoURLLabel;
    dbDetails.type = 'MongoDB';
  });
};

// this is here so that the test for document root will work. the actual doc root is react served by virtue of line 4 above
app.get('/', function (req, res) {
 res.json({message: "ok"}); 
});


// the live back-end routes::

app.get('/register/', (req, res, next) => {  
  if (!db) { initDb(function(err){}); }
  db.collection('registrations').find({$and : [{'activity': {$gt : ""}}, {'company': {$gt : ""}}]}).toArray((err, results) => {
      if (err) return console.log(err)
      res.send(results) 
    })
});

app.post('/register/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  db.collection('registrations').save(req.body, (err, result) => {
    if (err) return console.log(err)
    res.json({message: "ok with this result: "+result})
  })
}); 

app.delete('/register/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  const {ObjectId} = require('mongodb'); 
  db.collection('registrations').deleteOne( { _id : ObjectId(req.query._id) }, (err, result) => {
      if (err) return console.log(err)
      res.json({message: "ok with this result: "+result})
  })
});

app.get('/activities/', (req, res, next) => {  
  if (!db) { initDb(function(err){}); }
  db.collection('activities').find({$and : [{'name': {$gt : ""}}, {'name': {$gt : ""}}]}).toArray((err, results) => {
      if (err) return console.log(err)
      res.send(results) 
    });
});

app.post('/activities/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  db.collection('activities').save(req.body, (err, result) => {
  if (err) return console.log(err)
  res.json({message: "ok with this result: "+result})
  })
});

app.delete('/activities/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  const {ObjectId} = require('mongodb'); 
  db.collection('activities').deleteOne( { _id : ObjectId(req.query._id) }, (err, result) => {
    if (err) return console.log(err)
    res.json({message: "ok with this result: "+result})
  })
});

app.get('/companies/', (req, res, next) => {  
  if (!db) { initDb(function(err){}); }
  db.collection('companies').find({$and : [{'name': {$gt : ""}}, {'name': {$gt : ""}}]}).toArray((err, results) => {
      if (err) return console.log(err)
      res.send(results) 
  });
});

app.post('/companies/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  db.collection('companies').save(req.body, (err, result) => {
      if (err) return console.log(err)
      res.json({message: "ok with this result: "+result})
  })
});

app.delete('/companies/', (req, res) => {
  if (!db) { initDb(function(err){}); }
  const {ObjectId} = require('mongodb'); 
  db.collection('companies').deleteOne( { _id : ObjectId(req.query._id) }, (err, result) => {
      if (err) return console.log(err)
      res.json({message: "ok with this result: "+result})
  })
});

// error handling
app.use(function(err, req, res){
  console.error(err.stack);
  res.status(500).send('Something bad happened with this request: '+req);
});
initDb(function(err){
  console.log('Error connecting to Mongo. Message:\n'+err);
});

app.listen(port, ip);

module.exports = app ;
